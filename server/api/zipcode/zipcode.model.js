'use strict';

var mongoose = require( 'mongoose' ),
    Schema = mongoose.Schema;

var ZipcodeSchema = new Schema( {
    id: { type: Number, unique: true },
    postcode: String,
    postcode_id: Number,
    pnum: Number,
    pchar: String,
    minnumber: Number,
    maxnumber: Number,
    numbertype: String,
    street: String,
    city: String,
    city_id: Number,
    municipality: String,
    municipality_id: Number,
    province: String,
    province_code: String,
    lat: Number,
    lon: Number,
    rd_x: Number,
    rd_y: Number,
    location_detail: String,
    changed_date: Date,
    loc: {
        type: [ Number ]  // [<longitude>, <latitude>]
    }
} );

ZipcodeSchema.pre( 'save', function( next ) {
    this.loc = [ this.lon, this.lat ]
    next();
} );

ZipcodeSchema.index( { loc: '2d' } );

module.exports = mongoose.model( 'Zipcode', ZipcodeSchema );