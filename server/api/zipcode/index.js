'use strict';

var express = require( 'express' );
var controller = require( './zipcode.controller' );

var router = express.Router();

router.get( '/', controller.index );
router.get( '/:id', controller.show );
router.get( '/closest/:lng/:lat', controller.closest );
//router.post( '/', controller.create );
//router.put( '/:id', controller.update );
//router.patch( '/:id', controller.update );
//router.delete( '/:id', controller.destroy );

module.exports = router;