'use strict';

var _ = require( 'lodash' );
var Zipcode = require( './zipcode.model' );

// Get list of zipcodes
exports.index = function( req, res ) {
    Zipcode.find( function( err, zipcodes ) {
        if( err ) {
            return handleError( res, err );
        }
        return res.status( 200 ).json( zipcodes );
    } );
};

// Get a single zipcode
exports.show = function( req, res ) {
    Zipcode.findById( req.params.id, function( err, zipcode ) {
        if( err ) {
            return handleError( res, err );
        }
        if( !zipcode ) {
            return res.status( 404 ).send( 'Not Found' );
        }
        return res.json( zipcode );
    } );
};

// Creates a new zipcode in the DB.
exports.create = function( req, res ) {

    var newZipcode = new Zipcode( req.body );
    newZipcode.save( function( err, zipcode ) {
        if( err ) {
            return handleError( res, err );
        }
        return res.status( 201 ).json( zipcode );
    } );
};

exports.closest = function( req, res ) {

    var lat = req.params.lat;
    var lng = req.params.lng;

    var limit = req.query.limit || 1;

    // get the max distance or set it to 8 kilometers
    var maxDistance = req.query.distance || 80;

    // we need to convert the distance to radians
    // the raduis of Earth is approximately 6371 kilometers
    maxDistance /= 6371;

    // get coordinates [ <longitude> , <latitude> ]
    var coords = [];
    coords[ 0 ] = lng;
    coords[ 1 ] = lat;

    // find a location
    Zipcode.find( {
        loc: {
            $near: coords,
            $maxDistance: maxDistance
        }
    } ).limit( limit ).exec( function( err, locations ) {
        if( err ) {
            console.log( err );
            return handleError( res, err );
        }
        var currentLocation = locations[ 0 ];

        res.status( 200 ).json( currentLocation );
    } );

};

// Updates an existing zipcode in the DB.
exports.update = function( req, res ) {
    if( req.body._id ) {
        delete req.body._id;
    }
    Zipcode.findById( req.params.id, function( err, zipcode ) {
        if( err ) {
            return handleError( res, err );
        }
        if( !zipcode ) {
            return res.status( 404 ).send( 'Not Found' );
        }
        var updated = _.merge( zipcode, req.body );
        updated.save( function( err ) {
            if( err ) {
                return handleError( res, err );
            }
            return res.status( 200 ).json( zipcode );
        } );
    } );
};

// Deletes a zipcode from the DB.
exports.destroy = function( req, res ) {
    Zipcode.findById( req.params.id, function( err, zipcode ) {
        if( err ) {
            return handleError( res, err );
        }
        if( !zipcode ) {
            return res.status( 404 ).send( 'Not Found' );
        }
        zipcode.remove( function( err ) {
            if( err ) {
                return handleError( res, err );
            }
            return res.status( 204 ).send( 'No Content' );
        } );
    } );
};

function handleError( res, err ) {
    return res.status( 500 ).send( err );
}