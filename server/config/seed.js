/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';


function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}


var User = require( '../api/user/user.model' );
var Zipcode = require( '../api/zipcode/zipcode.model' );
var assert = require('assert')

User.find( {} ).remove( function() {
    User.create( {
            provider: 'local',
            name: 'Test User',
            email: 'test@test.com',
            password: 'test'
        }, {
            provider: 'local',
            role: 'admin',
            name: 'Admin',
            email: 'admin@admin.com',
            password: 'admin'
        }, function() {
            console.log( 'finished populating users' );
        }
    );
} );

Zipcode.findOne( {}, function( err, obj ) {
    if ( isEmpty( obj )){
        console.warn("Import ZIPCODE data first");
        console.warn("Look in the data directory of the project")
    };
} );


