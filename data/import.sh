#! bin/bash

#
# Data source : http://www.postcodedata.nl/download/
# Direct: link: http://download.postcodedata.nl/data/postcode_NL.csv.zip
# Version: 2014-04-10 14:44:11

if [ ! -f postcode_NL.csvls ]; then
    echo "File not found!"
    echo "Downloading now!"
    cd data
    wget http://download.postcodedata.nl/data/postcode_NL.csv.zip
    unzip postcode_NL.csv.zip
    rm postcode_NL.csv.zip
    python import_mongo.py postcode_NL.csv
    cd ..
fi
