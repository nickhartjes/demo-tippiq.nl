import csv
import sys
import json
from pymongo import MongoClient, GEO2D
import pprint

client = MongoClient('localhost', 27017)
db = client['demotippiqnl-dev']
collection = db['zipcodes']

fieldnames = [
    "id",
    "postcode",
    "postcode_id",
    "pnum",
    "pchar",
    "minnumber",
    "maxnumber",
    "numbertype",
    "street",
    "city",
    "city_id",
    "municipality",
    "municipality_id",
    "province",
    "province_code",
    "lat",
    "lon",
    "rd_x",
    "rd_y",
    "location_detail",
    "changed_date"
]


def convert(filename):
    csv_filename = filename[0]
    print "Opening CSV file: ", csv_filename
    f = open(csv_filename, 'r')
    csv_reader = csv.DictReader(f, fieldnames=fieldnames, delimiter=';', quotechar='"')
    count = 0

    collection.ensure_index([("loc", GEO2D)])
    for row in csv_reader:
      row['loc'] = []
      row['loc'].append(float(row['lon']))
      row['loc'].append(float(row['lat']))

      collection.insert(row);

      if count % 1000 == 0:
        print(count)
      count = count + 1

if __name__ == "__main__":
    convert(sys.argv[1:])
