'use strict';

angular.module( 'demoTippiqnlApp' )
    .filter( 'currentDate', function( $filter ) {
        return function( inputFormat ) {
            return $filter( 'date' )( new Date, inputFormat )
        };
    } );
