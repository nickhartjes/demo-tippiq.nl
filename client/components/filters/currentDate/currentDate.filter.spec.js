'use strict';

describe( 'Filter: currentDate', function() {

    // load the filter's module
    beforeEach( module( 'demoTippiqnlApp' ) );

    // initialize a new instance of the filter before each test
    var currentDate;
    beforeEach( inject( function( $filter ) {
        currentDate = $filter( 'currentDate' );
    } ) );

    it( 'should return the current year we live ih "currentDate filter:"', function() {
        var format = 'yyyy';
        expect( currentDate( format ) ).toBe( new Date().getFullYear().toString() );
    } );

} );
