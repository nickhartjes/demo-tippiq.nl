'use strict';

angular.module( 'demoTippiqnlApp' )
    .filter( 'zipcode', function() {
        return function( input ) {
            return input ? input.toString() : ""
        };
    } );
