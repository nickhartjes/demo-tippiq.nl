'use strict';

describe( 'Filter: zipcode', function() {

    // load the filter's module
    beforeEach( module( 'demoTippiqnlApp' ) );

    // initialize a new instance of the filter before each test
    var zipcode;
    beforeEach( inject( function( $filter ) {
        zipcode = $filter( 'zipcode' );
    } ) );

    it( 'should return the input to string', function() {
        var input = 1122;
        expect( zipcode( input ) ).toBe( input.toString() );
    } );

} );
