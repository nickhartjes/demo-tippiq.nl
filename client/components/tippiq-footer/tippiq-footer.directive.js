'use strict';

angular.module( 'demoTippiqnlApp' )
    .directive( 'tippiqFooter', function() {
        return {
            templateUrl: 'components/tippiq-footer/tippiq-footer.html',
            restrict: 'EA',
            controller: 'TippiqFooterCtrl'
        };
    } );