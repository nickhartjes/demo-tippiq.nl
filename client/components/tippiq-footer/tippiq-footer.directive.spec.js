'use strict';

describe( 'Directive: tippiqFooter', function() {

    // load the directive's module and view
    beforeEach( function() {
        module( 'my.templates' ),
        module( 'demoTippiqnlApp' )
    } );

    var element, scope;

    beforeEach( inject( function( $rootScope ) {
        scope = $rootScope.$new();
    } ) );

    it( 'should make hidden footer visible', inject( function( $compile ) {
        element = angular.element( '<tippiq-footer></tippiq-footer>' );
        element = $compile( element )( scope );
        scope.$apply();
        expect( element.text() ).toContain( 'initiatief' );
    } ) );
} );