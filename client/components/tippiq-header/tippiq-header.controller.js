'use strict';

angular.module( 'demoTippiqnlApp' )
    .controller( 'TippiqHeaderCtrl', function( $scope ) {
        $scope.btnBackClicked = function() {
            window.history.back();
        };

    } );