'use strict';

describe( 'Directive: tippiqHeader', function() {

    // load the directive's module and view
    beforeEach(
        module( 'my.templates' ),
        module( 'demoTippiqnlApp' )
        //module( 'components/tippiq-header/tippiq-header.html' )
    );

    var element, scope;

    beforeEach( inject( function( $rootScope ) {
        scope = $rootScope.$new();
    } ) );

    it( 'should make header element visible', inject( function( $compile ) {
        element = angular.element( '<tippiq-header></tippiq-header>' );
        element = $compile( element )( scope );
        scope.$apply();
        expect( element.text() ).toContain('beta');
    } ) );

    //var $compile,
    //    $rootScope;
    //
    //// Load the myApp module, which contains the directive
    //beforeEach(
    //    //angular.mock.module( 'ngMockE2E' ),
    //    module('my.templates'),
    //    module( 'demoTippiqnlApp' ),
    //    module( 'components/tippiq-header/tippiq-header.html' )
    //);
    //
    //// Store references to $rootScope and $compile
    //// so they are available to all tests in this describe block
    //beforeEach( inject( function( _$compile_, _$rootScope_ ) {
    //    // The injector unwraps the underscores (_) from around the parameter names when matching
    //    $compile = _$compile_;
    //    $rootScope = _$rootScope_;
    //} ) );
    //
    //it( 'Replaces the element with the appropriate content', function() {
    //    // Compile a piece of HTML containing the directive
    //    var element = $compile( "<tippiq-header></tippiq-header>" )( $rootScope );
    //    // fire all the watches, so the scope expression {{1 + 1}} will be evaluated
    //    $rootScope.$digest();
    //    // Check that the compiled element contains the templated content
    //    console.log( element );
    //    expect( element.text() ).toContain( '<a href="/" target="_self" class="logo-container logo-full hidden-xs" title="Tippiq"><img src="assets/images/logo/tippiq-logo-black-xs_@2.png" class="logo-image" alt="Tippiq"/> <span class="logo-betabadge">beta</span></a>' );
    //} );
} );