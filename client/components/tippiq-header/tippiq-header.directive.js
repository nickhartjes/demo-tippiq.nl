'use strict';

angular.module( 'demoTippiqnlApp' )
    .directive( 'tippiqHeader', function() {
        return {
            templateUrl: 'components/tippiq-header/tippiq-header.html',
            restrict: 'EA',
            controller: 'TippiqHeaderCtrl'
        };
    } );