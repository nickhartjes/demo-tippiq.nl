'use strict';

describe('Service: Zipcode', function () {

  // load the service's module
  beforeEach(module('demoTippiqnlApp'));

  // instantiate service
  var Zipcode;
  beforeEach(inject(function (_Zipcode_) {
    Zipcode = _Zipcode_;
  }));

  it('should do something', function () {
    expect(!!Zipcode).toBe(true);
  });

});
