'use strict';

angular.module('demoTippiqnlApp')
  .service('Zipcode', function ($resource) {
        return $resource( '/api/zipcodes/:id', { id: '@_id' }, {
            closest: {
                url: '/api/zipcodes/closest/:lng/:lat',
                method: 'GET'
            }
        } );
  });
