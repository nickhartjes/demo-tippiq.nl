'use strict';

angular.module( 'demoTippiqnlApp' )
    .config( function( $stateProvider ) {
        $stateProvider
            .state( 'main', {
                url: '/',
                templateUrl: 'app/main/main.html',
                controller: 'MainCtrl'
            } );
    } );