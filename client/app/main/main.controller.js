'use strict';

angular.module( 'demoTippiqnlApp' )
    .controller( 'MainCtrl', function( $geolocation, $scope, $http, Zipcode ) {


        $scope.media = [ {
            mimeType: "image/png",
            description: "wanneer je de auto even niet voor de deur kan parkeren",
            src: "/assets/images/carousel/bg-lighter-ratio-01-workroad.png"
        }, {
            mimeType: "image/png",
            description: "welke buren wel wat hulp kunnen gebruiken",
            src: "/assets/images/carousel/bg-lighter-ratio-02-help.png"
        }, {
            mimeType: "image/png",
            description: "wie een zaag te leen heeft",
            src: "/assets/images/carousel/bg-lighter-ratio-03-lenen.png"
        }, {
            mimeType: "image/png",
            description: "welke verbouwingen worden gepland",
            src: "/assets/images/carousel/bg-lighter-ratio-04-vergunning.png"
        }, {
            mimeType: "image/png",
            description: "waar je de politie mee kunt helpen",
            src: "/assets/images/carousel/bg-lighter-ratio-05-police.png"
        }, {
            mimeType: "image/png",
            description: "wanneer je de wasmachine even niet kan gebruiken",
            src: "/assets/images/carousel/bg-lighter-ratio-06-strooming.png"
        } ]

        $scope.placeholder = 'je postcode (bijv. 1234AB)';
        $scope.currentSlide = 0;
        $scope.whathappens = $scope.media[ $scope.currentSlide ].description;
        $scope.slickHandle = {}
        $scope.slickConfigLoaded = true;

        $scope.init = function() {
            $geolocation.getCurrentPosition( {
                timeout: 1000
            } ).then( function( position ) {
                Zipcode.closest( { lat: position.coords.latitude, lng: position.coords.longitude }, function( zipcode ) {;
                    $scope.placeholder = 'Is je postcode toevallig ' + zipcode.pnum + ' ' + zipcode.pchar + '  ??'

                } );

            } );
        };

        $scope.slickConfig = {
            autoplay: true,
            infinite: true,
            draggable: false,
            autoplaySpeed: 3000,
            method: {},
            event: {
                beforeChange: function( event, slick, currentSlide, nextSlide ) {
                },
                afterChange: function( event, slick, currentSlide, nextSlide ) {
                    $scope.whathappens = $scope.media[ currentSlide ].description;
                    $scope.$apply();
                }
            }
        };

        $scope.init();
    } );
