'use strict';

angular.module('demoTippiqnlApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('cookies', {
        url: '/cookies',
        templateUrl: 'app/cookies/cookies.html',
        controller: 'CookiesCtrl'
      });
  });